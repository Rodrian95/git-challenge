FROM node:16 as builder

ENV NODE_VERSION 16

RUN npm install -g typescript
RUN npm install -g ts-node
RUN npm i -g nodemon
RUN npm i -g pm2
RUN mkdir -p /app
WORKDIR /app
ENV NODE_ENV production

COPY package.json ./
COPY tsconfig.json ./
COPY .env.* ./
RUN npm install --production
COPY src ./src
COPY . ./

FROM node:16
RUN npm install -g ts-node
RUN npm install -g typescript
RUN npm i -g nodemon
RUN npm i -g pm2
RUN npm i -g pg
RUN mkdir -p /app
WORKDIR /app
RUN npm install --save @types/debug
RUN npm i --save-dev @types/express
RUN npm i --save-dev @types/morgan
RUN npm i --save-dev @types/body-parser
RUN npm i --save-dev @types/cors
RUN npm i --save-dev @types/jsonwebtoken
RUN npm i --save-dev @types/debug
RUN npm i --save-dev @types/pg

COPY package.json ./
COPY tsconfig.json ./
COPY .env.* ./

COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/src ./src
COPY --from=builder /app/tsconfig.json ./
COPY --from=builder /app/.env.* ./

EXPOSE 8000
CMD [ "npm", "run", "serve" ]
