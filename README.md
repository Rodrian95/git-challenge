**Prueba Guros Validacion de ADN**<br>
 En esta prueba se creo un servicio en el que se detectan las mutaciones de una cadena de ADN

 **Base de Datos**<br>
 La base de datos que se utilizo fue PostgreSQL

 **Ejecucion en local**<br>
 
1. Instalamos librerias corriendo el siguiente comando<br>
`npm i`

2. Corremos el siguiente comando para modo desarrollo<br>
`npm run dev`

**URLS**<br>
Las urls fueron protegidas con JWT <br>


1. **Mutation**<br>
POST/ http://localhost:8000/guros/mutation <br>

2. **Stats**<br>
GET/ http://localhost:8000/guros/stats <br>

3. **Auth**
GET/ http://localhost:8000/guros/auth <br>
Esta peticion nos devuelve un JTW el cual usamos en el header de Authorization para hacer las peticiones a las demas URLS

**EJEMPLOS**<br>
**CURL /MUTATION**
`curl --location --request POST 'https://guros-challengemx.herokuapp.com/guros/mutation' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqd3QiOiJwcnVlYmEiLCJpYXQiOjE2MzI5NzU3ODUsImV4cCI6MTYzMjk3NTg3MX0.xLX-unzOCBDQnYZ2LYvqH8lJIANQ-UqzF7oIfs1WQLY' \
--header 'Content-Type: application/json' \
--data-raw '{
  "dna":["AGTAGG","TGTAAC","AGACGG","ATGTAT","TATTCA","CCACAG"]
}`<br>
**CURL /STATS**<br>
`curl --location --request GET 'https://guros-challengemx.herokuapp.com/guros/stats' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqd3QiOiJwcnVlYmEiLCJpYXQiOjE2MzI5NzYzMDcsImV4cCI6MTYzMjk3NjM5M30.1q5Td7UBoxTTxUsb-y2NopoJUfVwGpRrpGIhz5zymw8'`



