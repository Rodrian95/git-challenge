import { Response, Request, NextFunction } from 'express';
import { verifyJWTToken } from './authUtils';

const NOT_AUTH_401 = 'Failed to authenticate token.';
const NO_JWT_403 = 'No token provided.';

const authMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {

  const { authorization = '' } = req.headers
  const jwt = authorization.split(' ')[1]
  if (jwt) {
    try {
      const decoded = await verifyJWTToken(jwt)
      next()
    } catch (error) {
      res.status(401).json({
        success: false,
        message: NOT_AUTH_401
      })
    }

  } else {
    return res.status(403).send({
      success: false,
      message: NO_JWT_403
    })
  }

};

export default authMiddleware;
