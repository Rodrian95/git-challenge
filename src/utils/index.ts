import { Console } from "console";
import { DnaStats } from "./../models/stats.interface";

type NormalizePort = (val: number | string) => number | string | boolean;
const dnaRegular = /A{4}|C{4}|G{4}|T{4}/gi;
export const normalizePort: NormalizePort = (val) => {
  const port: number = typeof val === "string" ? parseInt(val, 10) : val;
  if (isNaN(port)) return val;
  else if (port >= 0) return port;
  else return false;
};

const mutationHorizontal = async (dna: string[]) => {
  let count = 0;
  for (let i = 0; i < dna.length; i++) {
    if (dna[i].match(dnaRegular)) {
      count++;
    }
  }
  console.log(count);
  return count;
};

const mutationVertical = async (dna: string[]) => {
  let count = 0;
  for (let i = 0; i < dna.length; i++) {
      let cadena = '';
      for (let j = 0; j < dna.length; j++) {
          cadena += dna[j][i]; 
      }
      if (cadena.match(dnaRegular)) {
          count++;
      }
  }
  return count;
};

const mutationDiagonalUp = async (dna: string[]) => {
  let count = 0;
  for (let i = 0; i <= 2 * (dna.length - 1); ++i) {
    let cad = "";
    for (let j = dna.length - 1; j >= 0; --j) {
      const x = i - j;
      if (x >= 0 && x < dna.length) {
        cad += dna[j][x];
      }
    }
    if (cad.match(dnaRegular) && cad.length > 3) {
      count ++;
    }
  }
  return count;
};

const mutationDiagonalDown = async (dna: string[]) => {
  let count = 0;
  for (let i = 0; i <= 2 * (dna.length - 1); ++i) {
    let cad = "";
    for (let j = dna.length - 1; j >= 0; --j) {
      const z = i - (dna.length - j);
      if (z >= 0 && z < dna.length) {
        cad += dna[j][z];
      }
    }
    if (cad.match(dnaRegular) && cad.length > 3) {
      count++;
    }
  }
  return count;
};

export const hasMutation = async (dna: string[]) => {
  let count = 0;
  try {
    count = await mutationHorizontal(dna);
    if (count > 0) {
      return true;
    }
    count = await mutationVertical(dna);
    if (count > 0) {
      return true;
    }
    
    count = await mutationDiagonalUp(dna);
    if (count > 0) {
      return true;
    }
    count = await mutationDiagonalDown(dna);
    if (count > 0) {
      return true;
    }
  } catch (err) {
    console.log("hasmutationerror:", err);
  }

  return false;
};

export const getStats = async (dna: any[]) => {
  let stats: DnaStats = {
    count_mutations: 0,
    count_no_mutation: 0,
    ratio: 0,
  };
  stats.count_mutations = dna.filter((item) => item.mutation === 'true').length;
  stats.count_no_mutation = dna.filter((item) => item.mutation === 'false').length;
  stats.ratio = Number(stats.count_mutations / (stats.count_mutations + stats.count_no_mutation));
  return stats;
};
