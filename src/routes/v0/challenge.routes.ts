import express from "express";
import ChallengeController from "../../controllers/challenge";
import authMiddleware from "../../middleware/authMiddleware";

export class ChallengeRouter {
  challenge = new ChallengeController()
  router: express.Router = express.Router();
  cosntructor() {
    this.init()
  }

  init = () => {
    this.router.post("/mutation",[authMiddleware], this.challenge.adn);
    this.router.get("/stats",[authMiddleware], this.challenge.stats);
    this.router.get("/auth", this.challenge.createJTW);
  };
}

const challengeRoutes = new ChallengeRouter()
challengeRoutes.init()
export default challengeRoutes.router
