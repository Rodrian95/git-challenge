import { Request, Response } from "express";
import * as utils from "../../utils/index";
import challService from "../../services/challenge/challenge.service";
import { ADN } from "../../models/adn.interface";
import * as auth from "../../middleware/authUtils";

class ChallengeController {
  constructor() {}

  async adn(req: Request, res: Response) {
    let dna: ADN = {
      dna: [],
      mutation: false,
    };

    try {
      const alladns = await challService.validateDna(req.body);
      if (alladns.rows.length > 0) {
        return res.status(403).send({ message: "Registro repetido intenta uno nuevo" });
      }
      const result = await utils.hasMutation(req.body.dna);

      if (result) {
        dna.dna = req.body.dna;
        dna.mutation = true;
        await challService.insertDna(dna);
        return res.status(200).send({ message: "Mutación encontrada" });
      } else {
        dna.dna = req.body.dna;
        dna.mutation = false;
        await challService.insertDna(dna);
        return res.status(403).send({ message: "Sin mutación" });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).send({ error });
    }
  }

  async stats(req: Request, res: Response) {
    try {
      const dnas = await challService.getDna();
      const stats = await utils.getStats(dnas.rows);
      res.status(200).send(stats);
    } catch (err) {
      res.status(500).send({ err });
    }
  }

  async createJTW(req: Request, res: Response) {
    const jwt = await auth.createJWTToken();
    res.status(200).send({jwt});
  }
}

export default ChallengeController;
