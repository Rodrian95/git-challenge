export interface DnaStats {
    count_mutations: number,
    count_no_mutation: number,
    ratio: number
}