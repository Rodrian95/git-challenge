import http from "http";
import debug from "debug";
import { normalizePort } from "./utils";
import dotenv from "dotenv";
import App from "./App";

debug("ts-express:server");
dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

const port = normalizePort(process.env.PORT || 3002);

const onError = (error: NodeJS.ErrnoException): void => {
  if (error.syscall !== "listen") throw error;
  const bind = typeof port === "string" ? "Pipe " + port : "Port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const onListening = (): void => {
  console.log(`listening on port ${port}!`);
  console.log(`${process.env.NODE_ENV} mode is ON`);
};

App.set("port", port);
const server = http.createServer(App);
server.listen(port);
server.on("error", onError);
server.on("listening", onListening);
